FROM alpine:3.8

RUN apk add --update --no-cache util-linux nodejs nodejs-npm

RUN mkdir coresciences

COPY . ./peccy.net

RUN cd peccy.net && \
      npm install && \
      npm run build

EXPOSE 80/tcp

CMD cd peccy.net && npm run start
