export default () => (
  <header className="navbar">
    <div className="container">
      <div className="navbar-brand">
        <a className="navbar-item">
          <img src="/static/img/logo-white-filled-blueStroke.png" alt="Logo"/>
        </a>
        <span className="navbar-burger burger" data-target="navbarMenuHeroC">
          <span></span>
          <span></span>
          <span></span>
        </span>
      </div>
      <div id="navbarMenuHeroC" className="navbar-menu">
        <div className="navbar-end">
          <a className="navbar-item" href="/" prefetch>
            Home
          </a>
          <a className="navbar-item">
            <div className="dropdown is-hoverable">
              <div className="dropdown-trigger">
                <span>Endeavours</span>
                <span className="icon is-small">
                  <i className="fas fa-angle-down" aria-hidden="true"></i>
                </span>
              </div>
              <div className="dropdown-menu" role="menu">
                <div className="dropdown-content">
                  <div className="dropdown-item">
                    <a href="//solinr.net" target="_blank">Solinr Hosting</a>
                  </div>
                  <div className="dropdown-item">
                    <a href="//capricious.design" target="_blank">Capricious Design</a>
                  </div>
                  <div className="dropdown-item">
                    <a href="//corelex.net" target="_blank">Corelex Gaming</a>
                  </div>
                  <div className="dropdown-item">
                    <a href="//uagpmc.com" target="_blank">Unnamed Arma Group</a>
                  </div>
                </div>
              </div>
            </div>
          </a>
          <a className="navbar-item" href="//status.peccy.net" target="_blank">
            Status
          </a>
          <span className="navbar-item">
            <a className="button is-success is-inverted">
              <span className="icon">
                <i className="fas fa-lock"></i>
              </span>
              <span>Login</span>
            </a>
          </span>
        </div>
      </div>
    </div>
  </header>
)
