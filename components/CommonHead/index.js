import Head from 'next/head'

export default () => (
  <Head>
    <meta charSet="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="author" content="Alex (@zeue)"/>
    <meta name="description" content="The IT platform powered by curiosity and innovation"/>
    <title>Peccy Networks - Innovative IT Operations</title>
    <link rel="icon" href="/static/img/logo-blue-filled.png"/>
    <link rel="stylesheet" href="/static/css/bulma.css"/>
    <link rel="stylesheet" href="/static/css/all.css"/>
    <link rel="stylesheet" href="/static/css/custom.css"/>
  </Head>
)
